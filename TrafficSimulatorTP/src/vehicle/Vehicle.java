package vehicle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

import excepciones.Excepciones;
import simulator.model.Junction;
import simulator.model.Road;
import simulator.model.SimulatedObject;
import simulator.model.VehicleStatus;

public class Vehicle extends SimulatedObject {


	protected int maximum_speed;
	private int current_speed = 0;;
	protected VehicleStatus status;
	protected int location = 0;
	protected List<Junction> itinerary;
	protected int contamination = 0;
	protected Road road;
	protected int total_contamination = 0;
	protected int total_travelled_distance = 0;
	protected int lastJunction = 0;
	
	
	
	public Vehicle(String id, int maximum_speed,  int contamination, List<Junction> itinerary
			) throws Excepciones {
		
		super(id);
		if(maximum_speed >= 0) {
			this.maximum_speed = maximum_speed;
		}else {
			throw new Excepciones(111);
		}
		
		if (contamination >= 0 && contamination<=10) {
			this.contamination=contamination;
		}else {
			throw new Excepciones(333);
		}
		if(itinerary.size()>= 2) {	
			this.itinerary = Collections.unmodifiableList(new ArrayList<>(itinerary));;
		
		}else {
			throw new Excepciones(777);
		}
		
		if(status == null)status = VehicleStatus.PENDING;
		
	}
	
	//getter & Setter
	public int getTravelletDistance() {
		return this.total_travelled_distance;
	}
	public int getTotalCont() {
		return this.total_contamination;
	}
	public List<Junction> getItinerary() {
		return itinerary;
	}

	public void setItinerary(List<Junction> itinerary) {
		this.itinerary = itinerary;
	}

	public Road getRoad() {
		return road;
	}

	public void setRoad(Road road) {
		this.road = road;
	}
	
	public int getCurrent_speed() {
		return current_speed;
	}

	public void setCurrent_speed(int current_speed) {
		this.current_speed = current_speed;
	}
	
	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public int getContamination() {
		return contamination;
	}
	
	public int setContamination() {
		return contamination;
	}

	public int getMaximum_speed() {
		return maximum_speed;
	}


	public void setMaximum_speed(int maximum_speed) {
		this.maximum_speed = maximum_speed;
	}

	public VehicleStatus getStatus() {
		return status;
	}

	public void setStatus(VehicleStatus status) {
		this.status = status;
	}


	//metodos
	public void setSpeed(int s) throws Excepciones { 
		if (s < 0) {
			throw new Excepciones(111);
		}else { 
			this.current_speed = Math.min(s,this.maximum_speed);
		}
	}
	
	public void setContamination(int c) throws Excepciones { 
		if (c >=0 && c<=10){
			this.contamination = c;
		}else {
			throw new Excepciones(333);
		}

	}
	
	@Override
	public void advance(int time) throws Excepciones {
		  
		  if(this.status == VehicleStatus.TRAVELING) {
		   this.lastJunction = this.location;
		   this.location = Math.min(this.location + this.current_speed, road.getLength());
		   road.addContamination (contamination * ((this.location-this.lastJunction)));
		   total_contamination += contamination * (this.location-this.lastJunction) ;
		   this.total_travelled_distance += (this.location-this.lastJunction);
		     
		   if (this.location == road.getLength()) {
		    
		    this.current_speed = 0;
		    status = VehicleStatus.WAITING;
		    road.getDest().enter(this);
		   }    
		   }
		 }
	
		// TODO Auto-generated method stub

	

	public void moveToNextRoad() throws Excepciones {
		  this.lastJunction += this.location; // siempre es ceros
		  if(this.status == VehicleStatus.PENDING){ // inicio del viaje
		   this.location = 0;
		   this.setSpeed(0);
		   road = itinerary.get(0).roadTo(itinerary.get(1));
		   road.enter(this);
		   this.status = VehicleStatus.TRAVELING;
		   
		  }else 
		   if(itinerary.indexOf(road.getDest())+1 == itinerary.size()){ // 
		    this.location = -1;
		    this.road.exit(this);
		    this.road = null;
		    this.status = VehicleStatus.ARRIVED;
		    this.current_speed = 0;
		  }else{ 
		   this.location = 0;
		   int temp_indexJuntion = itinerary.indexOf(road.getDest())+1;
		   this.road.exit(this);
		   road = road.getDest().roadTo(itinerary.get(temp_indexJuntion));
		   this.road.enter(this);
		   this.status = VehicleStatus.TRAVELING;
		  }
		 }

	@Override
	public JSONObject report() 
	{ 
		JSONObject vh = new JSONObject();
		
		vh.put("id", this._id);
		vh.put("speed", this.current_speed);
		vh.put("distance", this.total_travelled_distance);
		vh.put("co2",this.total_contamination);
		vh.put("class",this.contamination );
		vh.put("status", this.status);
		vh.put("road", this.road);
		if(this.location !=-1)vh.put("location", this.location);
		// TODO Auto-generated method stub
		return vh;
	}

	public int getContClass() {
		// TODO Auto-generated method stub
		return this.contamination;
	}


	

	
 
}
