package simulator.factories;

import simulator.model.Event;
import simulator.model.NewCityRoadEvent;
import simulator.model.Weather;

public class NewCityRoadEventBuilder extends NewRoadEventBuilder{

	public NewCityRoadEventBuilder() {
		super("new_city_road");
		// TODO Auto-generated constructor stub
	}

	@Override
	public Event createRoad(int time, String id, String src, String dest, int length, int co2limit, int maxspeed,
			Weather w) {
		// TODO Auto-generated method stub
		return new NewCityRoadEvent(time, id, src, dest, length, co2limit, maxspeed, w);
		}	
}

	