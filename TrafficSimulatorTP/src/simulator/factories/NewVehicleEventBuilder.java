package simulator.factories;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import simulator.model.Event;
import simulator.model.NewVehicleEvent;



public class NewVehicleEventBuilder extends Builder<Event>{

	public NewVehicleEventBuilder() {
		super("new_vehicle");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Event createTheInstance(JSONObject data) {
		
		
		
		//NewVehicleEvent nve;
		int time = data.getInt("time");
		String id = data.getString("id");
		int maxspeed = data.getInt("maxspeed");
		int contClass = data.getInt("class");
		List<String> itinerary = new ArrayList<>();		
		
		for(int i = 0;  i < data.getJSONArray("itinerary").length(); i++ ) {
	
			itinerary.add(data.getJSONArray("itinerary").getString(i));
		}
		return new NewVehicleEvent(time, id, maxspeed, contClass, itinerary);
		//return nve;
		
	}

}
