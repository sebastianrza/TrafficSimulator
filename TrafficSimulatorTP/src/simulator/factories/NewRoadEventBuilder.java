package simulator.factories;
import org.json.JSONObject;
import simulator.model.Event;
import simulator.model.Weather;

public abstract class NewRoadEventBuilder extends Builder<Event> {


	public NewRoadEventBuilder(String type) {
		super(type);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Event createTheInstance(JSONObject data) {
		//if(!data.isEmpty()) {
			
			int time = data.getInt("time");
			String id = data.getString("id");
			String src = data.getString("src");
			String dest = data.getString("dest");
			int length = data.getInt("length");
			int co2limit = data.getInt("co2limit");
			int maxspeed = data.getInt("maxspeed");
			String weather = data.getString("weather");
			Weather w = Weather.valueOf(weather.toUpperCase());
	
			return createRoad(time, id, src, dest, length, co2limit, maxspeed, w);
		
			
	}
		
	public abstract Event createRoad(int time, String id, String src, 
			String dest, int length, int co2limit, int maxspeed,Weather w);
}
