package simulator.model;
import excepciones.Excepciones;
import vehicle.Vehicle;

public class CityRoad extends Road{

	

	protected CityRoad(String id, Junction srcJunc, Junction destJunc, int length, int contLimit, int maximum_speed,
			Weather weather) throws Excepciones {
		super(id, srcJunc, destJunc, maximum_speed, contLimit, length, weather);
		// TODO Auto-generated constructor stub
	}

	@Override
	void reduceTotalContamination() {
		if(this.weather == Weather.WINDY || this.weather.equals(Weather.STORM)) {
			if(this.total_contamination-10 < 0) {
				this.total_contamination = 0;
			}else {
			this.total_contamination = this.total_contamination - 10;
			}
			
		}else {
			if(this.total_contamination - 2 < 0){
				this.total_contamination = 0;
			}else{
				this.total_contamination = this.total_contamination - 2;
			}
		}
		
		// TODO Auto-generated method stub
		
	}

	@Override
	void updateSpeedLimit() {
		this.current_speed_limit = this.maximum_speed;
		// TODO Auto-generated method stub
		
	}

	@Override
	int calculateVehicleSpeed(Vehicle v) {
		int s = this.current_speed_limit;
		int f = v.getContamination();
		int Current_speed = v.getCurrent_speed();
		Current_speed = (int)(((11.0-f)/11.0)*s);
		return Current_speed;
		
		// TODO Auto-generated method stub
		
	}


}
