package simulator.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONObject;
import excepciones.Excepciones;
import simulator.misc.SortedArrayList;
import vehicle.Vehicle;


public class TrafficSimulator implements Observable<TrafficSimObserver>{
	protected List<TrafficSimObserver> listObserver = new ArrayList<>();
	protected RoadMap roadmap;
	protected List<Event> list_event = new SortedArrayList<>();
	private int simulation_time;
	
	public TrafficSimulator() {
		this.roadmap = new RoadMap(new ArrayList<Junction>(), new ArrayList<Road>(),
				new ArrayList<Vehicle>(), new HashMap<String, Junction>(), new HashMap<String, Road>(),
				new HashMap<String, Vehicle>());
		
	}
	
	public void addEvent (Event e) {
		this.list_event.add(e);
		for (TrafficSimObserver o : listObserver) {
            o.onEventAdded(roadmap, list_event, e, getSimulation_time());
        }
	}
	
	public RoadMap getRoadMap(){
		return roadmap;
	}
	
	public List<Event> getEvents(){
		return list_event;
	}
	
	public void advance() throws Excepciones {
		this.setSimulation_time(this.getSimulation_time() + 1);
		for (TrafficSimObserver o : listObserver) {
            o.onAdvanceStart(roadmap, list_event, getSimulation_time());
        }
		List<Event> removeEvents = new LinkedList<Event>();
		for(int i=0; i < this.list_event.size();i++){
			if(this.list_event.get(i)._time == this.getSimulation_time()){
				removeEvents.add(this.list_event.get(i));
			}
		}
		if(removeEvents.size()!= 0){
			for(Event e: removeEvents){
			    e.execute(roadmap);
			} 
		this.list_event.removeAll(removeEvents);
		}		  
		for(Junction jun : this.roadmap.getJunctions()) {
			jun.advance(this.getSimulation_time());
		}
		for(Road ro : this.roadmap.getRoads()) {
			ro.advance(this.getSimulation_time());
		}
		for (TrafficSimObserver o : listObserver) {
            o.onAdvanceEnd(roadmap, list_event, getSimulation_time());
        }
	}
	
	public void reset() {
		
		this.roadmap.reset(); 
		this.list_event.clear();
		this.setSimulation_time(0);
		for (TrafficSimObserver o : listObserver) {
            o.onReset(roadmap, list_event, getSimulation_time());
        }
	}
	
	public JSONObject report(){
		JSONObject ts = new JSONObject();
		
		ts.put("time", this.getSimulation_time());
		ts.put("state", roadmap.report());
		return ts;	
	}

	@Override
	public void addObserver(TrafficSimObserver o) {
		this.listObserver.add(o);
		o.onRegister(roadmap, list_event, getSimulation_time());
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeObserver(TrafficSimObserver o) {
		this.listObserver.remove(o);
		// TODO Auto-generated method stub
		
	}

	public int getSimulation_time() {
		return simulation_time;
	}

	public void setSimulation_time(int simulation_time) {
		this.simulation_time = simulation_time;
	}	
	
	
}
