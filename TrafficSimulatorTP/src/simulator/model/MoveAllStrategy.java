package simulator.model;

import java.util.LinkedList;
import java.util.List;

import vehicle.Vehicle;

public class MoveAllStrategy implements DequeuingStrategy{

	@Override
	public List<Vehicle> dequeue(List<Vehicle> q) {
		List<Vehicle> Copydequeue = new LinkedList<>(q);
		
		return Copydequeue;
	}
	public String toString(){
		return "Move_all_dqs";
	}

}