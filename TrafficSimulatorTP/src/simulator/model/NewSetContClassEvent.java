package simulator.model;

import java.util.List;

import excepciones.Excepciones;
import simulator.misc.Pair;

public class NewSetContClassEvent extends Event{
	protected List<Pair<String,Integer>> cs;
	
	public NewSetContClassEvent(int time, List<Pair<String, Integer>> cs) throws Excepciones {
		super(time);
		if(cs == null) {
			throw new Excepciones(555);
		}else {
			this.cs = cs;
		}
	}
	@Override
	void execute(RoadMap map) throws Excepciones {
		for(Pair<String,Integer> c : cs){ //toma pite el vehiculo al map y cambia la contaminacion
			if(map.map_vehicle.get(c.getFirst()) != null) {
				map.getVehicle(c.getFirst()).setContamination(c.getSecond());
			}else {
				throw new Excepciones(555);
			}
		}
		// TODO Auto-generated method stub
	}
	public String toString() {
		String sC = "New contamination class: [";
		for(Pair<String,Integer> c : cs){
			sC += "(" + c.getFirst() + ", " + c.getSecond() + "), ";
		}
		return sC + "]";
	}
}
