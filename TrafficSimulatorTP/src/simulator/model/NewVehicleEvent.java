package simulator.model;

import java.util.ArrayList;
import java.util.List;

import excepciones.Excepciones;
import vehicle.Vehicle;

public class NewVehicleEvent extends Event {

	protected String id;
	protected int maxSpeed;
	protected int contClass;
	protected List<String> itinerary;
	protected int time;
	
	public NewVehicleEvent(int time, String id, int maxSpeed,
			int contClass, List<String> itinerary) {
		
		super(time);
		this.id = id;
		this.maxSpeed = maxSpeed;
		this.contClass = contClass;
		this.itinerary = itinerary;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	void execute(RoadMap map) throws Excepciones {
		ArrayList<Junction> listJunc = new ArrayList<>();
		for(int i = 0; i<this.itinerary.size(); i++) { //
			String temp = this.itinerary.get(i);
			listJunc.add(map.getJunction(temp));
		}
		Vehicle v = new Vehicle(this.id, this.maxSpeed, this.contClass, listJunc);
		map.addVehicle(v);
		v.moveToNextRoad();
		// TODO Auto-generated method stub	
	}
	@Override
	public String toString() {
		
		return "New Vehicle '"+id+"', '"+maxSpeed+"', '"+contClass+"', '"+time+"'";
	}
}
	
	


