package simulator.model;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import excepciones.Excepciones;
import vehicle.Vehicle;


public class Junction extends SimulatedObject {

	protected List<Road> incoming_roads = new LinkedList<>(); //Este cruce es el destieno de estas carreteras
	protected Map<Junction,Road> outgoing_roads = new HashMap<>(); //this --Road-->Jun
	protected List<List<Vehicle>> queue; 
	protected int green_traffic_light = -1;
	protected int traffic_light_change = 0;
	protected LightSwitchingStrategy lsStrategy;
	protected DequeuingStrategy dqStrategy;
	protected int xCoor;
	protected int yCoor;			
	
	protected Junction(String id, LightSwitchingStrategy lsStrategy, DequeuingStrategy dqStrategy,
			int xCoor, int yCoor) throws Excepciones {
		super(id);
		
		if(lsStrategy != null) {
		this.lsStrategy = lsStrategy;
		}else {
			throw new Excepciones(555);
		}
		if(dqStrategy != null) {
		this.dqStrategy = dqStrategy;
		}else {
			throw new Excepciones(555);
		}
		if(xCoor >= 0) {
		this.xCoor = xCoor;
		}else {
			throw new Excepciones(666);
		}
		if(yCoor >=0) {
		this.yCoor = yCoor;
		}else {
			throw new Excepciones(666);
		}
		
		queue = new LinkedList<List<Vehicle>>();
		
		// TODO Auto-generated constructor stub
	}
	
	public List<Vehicle> getQueue(Road r){
		for(int i=0; i < this.incoming_roads.size(); i++){
			if(this.incoming_roads.get(i).equals(r)) {return this.queue.get(i);}
		}
		return null;
	}
	
	public void addIncommingRoad(Road r) {
		if(r.getDest()._id == this._id){
			incoming_roads.add(r);
			queue.add(new LinkedList<Vehicle>());
		}	
	}
	
	public void addOutGoingRoad(Road r) throws Excepciones{
			if(outgoing_roads.containsKey(r.getDest())){ 
				System.out.println(r.getDest()._id);
				throw new Excepciones(666);
			}else{
				if(r.getSrc()._id == this._id){
					outgoing_roads.put(r.getDest(), r);
				}else{
					throw new Excepciones(666);
				}
			}
		}
	
	public void enter (Vehicle v) throws Excepciones { //a�ade el veh�culo v a la cola de la carretera r, r es la carretera actual del carro
		this.queue.get(this.incoming_roads.indexOf(v.getRoad())).add(v);
	}
	
	public Road roadTo(Junction j) {
		return outgoing_roads.get(j);
		 
	}

	@Override
	public void advance(int time) throws Excepciones {
		  
		  if(this.green_traffic_light != -1){ //nuevo junctAdvance
			  for(Vehicle v: this.queue.get(this.green_traffic_light)){
				  v.moveToNextRoad();
				  this.queue.get(this.green_traffic_light).remove(v);
			  }
		  }
		  int light = this.lsStrategy.chooseNextGreen(incoming_roads, this.queue, this.green_traffic_light, this.traffic_light_change, time);
		  if(light != green_traffic_light) {
		   this.green_traffic_light = light;
		   this.traffic_light_change = time;
		  }
	}
		
		// TODO Auto-generated method stub

	@Override
	public JSONObject report() {
		
		 JSONObject junc = new JSONObject();
		 JSONArray Queus = new JSONArray();
		 JSONObject road = new JSONObject();
		 JSONArray vacio = new JSONArray();
		 JSONArray prueba = new JSONArray();
		  if(this.incoming_roads.size() <= 0){
		  }else{
		   for(int i = 0; i<=this.incoming_roads.size()-1; i++) {
		    road.put("road",this.incoming_roads.get(i)._id);
		    if(this.queue.size() <= 0){
		     road.put("vehicles", "");
		    }else{
		     if(queue.get(i).size() == 0){
		      road.put("vehicles",vacio);
		     }else{
		      for(int j = 0; j <= queue.get(i).size()-1; j++) {
		    	prueba.put(queue.get(i).get(j)._id);
		       road.put("vehicles", prueba);
		      }
		     }
		    }
		    Queus.put(road);
		    road = new JSONObject();
		   }
		  
		  }
		  junc.put("id", this._id);
		  if (this.green_traffic_light == -1) {
		   junc.put("green", "none");
		  }else {
		    junc.put("green", this.incoming_roads.get(this.green_traffic_light)._id);
		  }
		  junc.put("queues", Queus);
		  // TODO Auto-generated method stub
		  return junc;
		 }

	public int getX() {
		// TODO Auto-generated method stub
		return xCoor;
	}
	public int getY() {
		// TODO Auto-generated method stub
		return yCoor;
	}
	public int getGreenLightIndex() {
		// TODO Auto-generated method stub
		return green_traffic_light;
	}

	public List<Road> getInRoads() {
		// TODO Auto-generated method stub
		return this.incoming_roads;
	}
	
	
}

