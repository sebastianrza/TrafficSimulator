package simulator.model;

import java.util.ArrayList;
import java.util.List;

import vehicle.Vehicle;

public class MoveFirstStrategy implements DequeuingStrategy{

	@Override
	public List<Vehicle> dequeue(List<Vehicle> q) {
		List<Vehicle> firstVehicle = new ArrayList<>();
		firstVehicle.add(q.get(0));
		return firstVehicle;
		
	}
	public String toString(){
		return "move_first_dqs";
	}

}
