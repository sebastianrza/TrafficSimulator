package simulator.model;
import org.json.JSONObject;

import excepciones.Excepciones;

public abstract class SimulatedObject {

	protected String _id;
	
	protected SimulatedObject(String id){
		if(id == null){
			throw new IllegalArgumentException("Simulated Object identifier cannot be null");
		}else 
			_id = id;
	}
	
	public String getId() {
		return _id;
	}

	
	@Override
	public String toString() {
		return _id;
	}

	public abstract void advance(int time) throws Excepciones;

	abstract public JSONObject report();
}
