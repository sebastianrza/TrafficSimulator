 package simulator.model;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import excepciones.Excepciones;
import vehicle.Vehicle;

public abstract class Road extends SimulatedObject {


	protected Junction src;
	protected Junction dest;
	protected int length = 0;
	protected int maximum_speed = 0;
	protected int current_speed_limit = this.maximum_speed;
	protected int contLimit = 0;
	protected Weather weather;
	protected int total_contamination = 0;
	protected List<Vehicle> vehicles = new LinkedList<Vehicle>();
	
	
	protected Road(String id, Junction src, Junction dest, int maximum_speed, int contLimit, int length,  
			Weather weather) throws Excepciones {
		super(id);
		if(src != null) {
		this.src = src;
		}else {
			throw new Excepciones(555);
		}
		if (dest !=null) {
		this.dest = dest;
		}else {
			throw new Excepciones(555);
		}
		if(length >=0) {
		this.length = length;
		}else {
			throw new Excepciones(111);
		}
		if (maximum_speed >=0 ) {
		this.maximum_speed = maximum_speed;
		}else {
			throw new Excepciones(111);
		}
		
		if(contLimit >=0) {
		this.contLimit = contLimit;
		}else {
			throw new Excepciones(222);
		}
		if(weather != null) {
		this.weather = weather;
		}else {
			throw new Excepciones(555);
		}
		// TODO Auto-generated constructor stub
	}
	
	//Getter and setter
	public int getCurrentSpeed() {
		return this.current_speed_limit;
	}
	public int getMaxSpeed() {
		return this.maximum_speed;
	}
	public Weather getWeather() {
		return this.weather;
	}
	public String getId() {
		return _id;
	}
	public Junction getSrc() {
		return this.src;
	}

	public void setSrc(Junction src) {
		this.src = src;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	public Junction getDest() {
		return this.dest;
	}

	public void setDest(Junction dest) {
		this.dest = dest;
	}

	//metodos
	
	public void enter(Vehicle v) throws Excepciones {
		if (v.getCurrent_speed() == 0 && v.getLocation() == 0) {
			this.vehicles.add(v);
		}else {
			throw new Excepciones(888);
		}
	}
	
	public void exit(Vehicle v) {
		this.vehicles.remove(v);	
	}
	
	void setWeather(Weather w) throws Excepciones {
		if(w != null) {
		this.weather = w;
		}else {
			throw new Excepciones(444);
		}
		
	}
	
	public void addContamination(int c)throws Excepciones {
		if (c >=0){
			this.total_contamination = this.total_contamination + c;
		}else {
			throw new Excepciones(222);
		}
	}
	
	abstract void reduceTotalContamination();
	
	abstract void updateSpeedLimit(); 
	
	abstract int calculateVehicleSpeed(Vehicle v);
	
	public void advance (int time) throws Excepciones {
		this.reduceTotalContamination();
		this.updateSpeedLimit();
		for(Vehicle v : vehicles){
			if(v.getStatus()!=VehicleStatus.WAITING) {
			v.setSpeed(calculateVehicleSpeed(v));
			v.advance(time);	
			}
		}
	}

	
	@Override
	public JSONObject report() {
		
		JSONObject ro = new JSONObject();
		JSONArray vh = new JSONArray();
		for(int i = 0; i <= vehicles.size()-1; i++){
			vh.put(vehicles.get(i)._id);
		}
		ro.put("id", this._id);
		ro.put("speedlimit", current_speed_limit);
		ro.put("co2", this.total_contamination);
		ro.put("weather",this.weather);
		ro.put("vehicles", vh);
		return ro;
	}

	public int getTotalCO2() {
		// TODO Auto-generated method stub
		return this.total_contamination;
	}

	public int getCO2Limit() {
		// TODO Auto-generated method stub
		return this.contLimit;
	}

	
		
}