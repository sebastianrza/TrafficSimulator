package simulator.model;
import excepciones.Excepciones;
import vehicle.Vehicle;

public class InterCityRoad extends Road{


	protected InterCityRoad(String id, Junction src, Junction dest, int length, int contLimit, int maximum_speed,
			Weather weather) throws Excepciones {
		super(id, src, dest, maximum_speed, contLimit, length, weather);
		// TODO Auto-generated constructor stub
	}

	@Override
	void reduceTotalContamination() {
		if(weather == Weather.SUNNY) 
		{
			total_contamination =(int)(((100.0-2)/100.0)*total_contamination);
			
		}else if(weather == Weather.CLOUDY) {
			total_contamination =(int)(((100.0-3)/100.0)*total_contamination);
		}
		else if(weather == Weather.RAINY) {
			total_contamination =(int)(((100.0-10)/100.0)*total_contamination);
		}
		else if(weather == Weather.WINDY) {
			total_contamination =(int)(((100.0-15)/100.0)*total_contamination);
		}
		else if(weather == Weather.STORM) {
			total_contamination =(int)(((100.0-20)/100.0)*total_contamination);
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	void updateSpeedLimit() {
		if(total_contamination > contLimit){
			current_speed_limit = (int)(maximum_speed*0.5);
		}else {
			current_speed_limit = maximum_speed;
		}
		// TODO Auto-generated method stub
	}


	@Override
	int calculateVehicleSpeed(Vehicle v) {
		 if(this.weather.equals(Weather.STORM)) {
			int Current_speed = (int) (this.current_speed_limit*0.8);
			return Current_speed;
		}else {
			int Current_speed = v.getCurrent_speed();
			Current_speed = this.current_speed_limit;
			return Current_speed;
		}
		// TODO Auto-generated method stub
	}	
}
