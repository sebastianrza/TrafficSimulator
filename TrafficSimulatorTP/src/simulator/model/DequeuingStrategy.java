package simulator.model;

import java.util.List;

import vehicle.Vehicle;

public interface DequeuingStrategy {

	List<Vehicle> dequeue (List<Vehicle> q);
	
	
}