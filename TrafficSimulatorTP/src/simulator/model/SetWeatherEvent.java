package simulator.model;
import java.util.List;
import excepciones.Excepciones;
import simulator.misc.Pair;

public class SetWeatherEvent extends Event{

	protected List<Pair<String, Weather>> ws;
	
	public SetWeatherEvent(int time, List<Pair<String, Weather>> ws) throws Excepciones {
		super(time);
		if(ws == null) {
			throw new Excepciones(555);
		}else {
			this.ws = ws;
			
		}
		// TODO Auto-generated constructor stub
	}

	@Override

	void execute(RoadMap map) throws Excepciones { // toma los road del mapa de roads y los id que tengo en Pair y concuerden con el mapa les cambio el wather 
		for(Pair<String,Weather> w : ws){
			if(map.map_road.get(w.getFirst()) != null) {
				map.getRoad(w.getFirst()).setWeather(w.getSecond());
			}else {
				throw new Excepciones(555);
			}
		}
			// TODO Auto-generated method stub	
	}
	public String toString() {
		String wS = "New Weather: [" ;
		for(Pair<String, Weather> p : ws){
			wS += "(" + p.getFirst() + ", " + p.getSecond()+"), ";
		}
		return wS+"]";
	}
	
}
