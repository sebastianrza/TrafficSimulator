package simulator.control;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import simulator.model.Event;
import simulator.model.RoadMap;
import simulator.model.TrafficSimObserver;
import simulator.view.ChangeCO2ClassDialog;
import simulator.view.ChangeWeatherDialog;

public class ControlPanel extends JPanel implements TrafficSimObserver{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton fileChose;
	private JButton changeClssCont;
	private JButton changeWeather;
	private JButton run;
	private JButton stop;
	private JSpinner ticks;
	private JButton exit;
	private JFileChooser file;
	private String fFilePath;
	private Controller ctrl;
	private boolean _stopped = false;
	private int time = 0;
	private int currentTime = 0;

	
    
	public ControlPanel(Controller ctrl){
		
		this.setLayout(new BorderLayout());
		
		this.ctrl = ctrl;
		
		this.ctrl.addObserver(this);
		
		JToolBar toolBar = new JToolBar("controlPanel");
		file =new JFileChooser(new File("resources/examples/"));
		fFilePath = new String();
		
		this.fileChose = new JButton ();
		this.fileChose.setToolTipText("Open a file");
		this.fileChose.setIcon(new ImageIcon("resources/icons/open.png"));
		toolBar.add(this.fileChose);
		
		toolBar.addSeparator();
		
		this.changeClssCont = new JButton ();
		this.changeClssCont.setToolTipText("change contamination");
		this.changeClssCont.setIcon(new ImageIcon("resources/icons/co2class.png"));
		toolBar.add(this.changeClssCont);
		
		this.changeWeather = new JButton ();
		this.changeWeather.setToolTipText("change the weather");
		this.changeWeather.setIcon(new ImageIcon("resources/icons/weather.png"));
		toolBar.add(this.changeWeather);
		
		this.run = new JButton ();
		this.run.setToolTipText("run simulation");
		this.run.setIcon(new ImageIcon("resources/icons/run.png"));
		toolBar.add(this.run);
		
		this.stop = new JButton ();
		this.stop.setToolTipText("stop simulation");
		this.stop.setIcon(new ImageIcon("resources/icons/stop.png"));
		toolBar.add(this.stop);
		
		toolBar.add( new JLabel("Ticks:"));
		
		this.ticks = new JSpinner(new SpinnerNumberModel(10, 1, 10000, 1));
		this.ticks.setToolTipText("Simulation tick to run: 1-10000");
		this.ticks.setMaximumSize(new Dimension(80, 40));
		this.ticks.setMinimumSize(new Dimension(80, 40));
		this.ticks.setPreferredSize(new Dimension(80, 40));
		toolBar.add(this.ticks);
		
		this.exit = new JButton ();
		this.exit.setToolTipText("exit simulation");
		this.exit.setIcon(new ImageIcon("resources/icons/exit.png"));
		
		toolBar.setFloatable(false);
		toolBar.setRollover(true);
		
		this.add(toolBar, BorderLayout.LINE_START);
		this.add(this.exit, BorderLayout.LINE_END);
		
		
		//ActionListener si cliquean los botones aqui van a parar
		this.fileChose.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e){ 
      
            	// invoke the showsOpenDialog function to show the save dialog 
                int r = file.showOpenDialog(null); 
                // if the user selects a file 
                if (r == JFileChooser.APPROVE_OPTION) { 
                    // set the label to the path of the selected file 
                    fFilePath = (file.getSelectedFile().getAbsolutePath());
                } 
                // if the user cancelled the operation 
                if (r == JFileChooser.CANCEL_OPTION)
                	fFilePath = null;
               if(fFilePath != null){
                try {
                	InputStream fileInStream = new FileInputStream(new File(fFilePath));
                	ctrl.reset();
                	ctrl.loadEvents(fileInStream);          		
                  
            		}catch (Exception ex) {
            			JOptionPane.showMessageDialog(null, "Archivo incorrecto no puede leerse", "Error al cargar archivo", JOptionPane.WARNING_MESSAGE);
            			ex.printStackTrace();
            		}// TODO complete this method to start the simulation
               }
            }
        });
		
		this.changeClssCont.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e){
            	
            	ChangeCO2ClassDialog co = new ChangeCO2ClassDialog(ctrl, (JFrame) SwingUtilities.getWindowAncestor(changeClssCont));	
            }
        });
			
		this.changeWeather.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e){
           
            	ChangeWeatherDialog f = new ChangeWeatherDialog(ctrl,(JFrame) SwingUtilities.getWindowAncestor(changeWeather));
            }
        });
		
		this.run.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e){
            	_stopped = false;
            	
            	time = (int)ticks.getValue() - currentTime;
            
            	run_sim(time);
            	
            }
        });
		
		this.stop.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e){
            	stop();
            }
        });
		
		this.exit.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e){
            
            	int option = JOptionPane.showConfirmDialog(null, "Si pulsa aceptar se cerrara el programa", "cerrar la simulacion", JOptionPane.OK_OPTION);
            	if(option ==  JOptionPane.OK_OPTION)
            		System.exit(0);
            }
        });
	}

	@Override
	public void onAdvanceStart(RoadMap map, List<Event> events, int time) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAdvanceEnd(RoadMap map, List<Event> events, int time) {
		// TODO Auto-generated method stub
		this.currentTime = time;
	}

	@Override
	public void onEventAdded(RoadMap map, List<Event> events, Event e, int time) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onReset(RoadMap map, List<Event> events, int time) {
		// TODO Auto-generated method stub
		this.currentTime = time;
	}

	@Override
	public void onRegister(RoadMap map, List<Event> events, int time) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onError(String err) {
		// TODO Auto-generated method stub
		
	}
	
	private void run_sim(int n) {
		JButtonStatus(false);
		if (n > 0 && !_stopped) {
			try {
				ctrl.run(1);
			} catch (Exception e) {
			// TODO show error message;
			_stopped = true;
			return;
		}
			SwingUtilities.invokeLater(new Runnable() {
			@Override
				public void run() {
					
					run_sim(n - 1);
				}
			});
		} else {
			currentTime = 0;
			time = 0;
			JButtonStatus(true);
			_stopped = true;
		}
	}
	private void stop() {
		JButtonStatus(true);
		_stopped = true;
	}
	
	protected void JButtonStatus(boolean b){
		fileChose.setEnabled(b);
    	changeClssCont.setEnabled(b);
    	changeWeather.setEnabled(b);
    	run.setEnabled(b);
    	ticks.setEnabled(b);
    	exit.setEnabled(b);
	}

	
	

}
