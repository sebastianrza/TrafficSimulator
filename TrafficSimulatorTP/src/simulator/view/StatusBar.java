package simulator.view;
import java.awt.FlowLayout;
import java.awt.Label;
import java.util.List;
import javax.swing.JPanel;
import simulator.control.Controller;
import simulator.model.Event;
import simulator.model.RoadMap;
import simulator.model.TrafficSimObserver;

public class StatusBar extends JPanel implements TrafficSimObserver {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int time = 0;
	private Event e;
	protected Label t;
	protected Label ev;
	
	public StatusBar(Controller ctrl){
		t = new Label();
		ev = new Label();
		
		ev.setText("Welcome!!");
		
		this.setLayout(new FlowLayout(FlowLayout.LEADING));
		ctrl.addObserver(this);
		
		this.add(new Label("Time: "));
		
		this.add(t);
		this.add(ev);
	}
	
	public void update(int time){
		this.time = time;
		
		t.setText("" + this.time);
		if(e != null ){
			ev.setText("" + e.toString());
			e = null;
		}else
			ev.setText("");
		
		
		this.paintAll(this.getGraphics());
		
	}

	@Override
	public void onAdvanceStart(RoadMap map, List<Event> events, int time) {
	}

	@Override
	public void onAdvanceEnd(RoadMap map, List<Event> events, int time) {
		update(time);
	}

	@Override
	public void onEventAdded(RoadMap map, List<Event> events, Event e, int time) {	
		this.e = e;
		update(time);
	}

	@Override
	public void onReset(RoadMap map, List<Event> events, int time) {
		// TODO Auto-generated method stub
		update(time);
	}

	@Override
	public void onRegister(RoadMap map, List<Event> events, int time) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onError(String err) {
		// TODO Auto-generated method stub
	}

}
