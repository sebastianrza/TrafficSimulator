package excepciones;


public class Excepciones extends Exception {
	
	
	private static final long serialVersionUID = 1L;
	
	private int Error;
	     
	    public Excepciones(int Error){
	        super();
	        this.Error=Error;
	    }
	     
	    @Override
	    public String getMessage(){
	         
	        String mensaje="";
	         
	        switch(Error){
	        	case 000:
	        		mensaje="Dos cruces iguales para una misma carretera";
	        		break;
	        	case 111:
	                mensaje="Error, La velocidad no puede ser negativa";
	                break;
	            case 222:
	            	mensaje = "la contaminacion no puede ser negativa";
	            	break;
	            case 333:
	            	mensaje = "la contaminacion debe ser entre 0 y 10";
	            	break;
	            case 444:
	            	mensaje = "Las condiciones atmosferica no pueden ser null";
	            	break;
	            case 555:
	            	mensaje = "No pueden ser null";
	            	break;
	            case 666:
	            	mensaje = "No puede ser negativo";
	            case 777:
	            	mensaje = "Tiene que tener longitud mayor o igual a 2";
	            case 888:
	            	mensaje = "El vehiculo no esta preparado para salir de la carretera";
	            case 10:
	            	mensaje = "Este id de Vehiculo no corresponde a ningun mapa de carretera";
	            case 11:
	            	mensaje = "Este id de Carretera no corresponde a ningun mapa de carretera";
	            case 12:
	            	mensaje = "Este id de Cruce no corresponde a ningun mapa de carretera";
	        }
	         
	        return mensaje;
	         
	    }
}
